import pyttsx3

def texto_a_voz(texto):
    # Inicializar el motor de síntesis de voz
    engine = pyttsx3.init()

    # Inicializar el motor de síntesis de voz
    engine = pyttsx3.init()

    # Obtener la lista de voces disponibles en el sistema
    voices = engine.getProperty('voices')

    # Imprimir información sobre cada voz disponible
    count=0
    for voice in voices:
        print("Nº:", count)
        print("Id:", voice.id)
        print("Nombre:", voice.name)
        print("Idioma:", voice.languages)
        print("Género:", voice.gender)
        print("--------------------------------------------------")
        count+=1

    # Seleccionar una voz específica del sistema (por ejemplo, la primera voz de la lista)
    # Puedes cambiar el índice de la lista según la voz que quieras utilizar
    engine.setProperty('voice', voices[0].id)


    engine.say(texto)
    
    # Reproducir la voz
    engine.runAndWait()

# Texto que quieres convertir a voz
texto = "Hola, este es un ejemplo de conversión de texto a voz en Python."

# Llamar a la función para convertir texto a voz
texto_a_voz(texto)