# Hack2Progress
Se ha iniciado el desarrollo de una interfaz para predecir la situación del fuego a futuro vista a los históricos, las condiciones del momento y cámaras, también analizará la situación actual con respecto a otras cuestiones y brindará a los usuarios una aplicación que les dará instrucciones en caso de urgencia médica antes de que lleguen los servicios de emergencia.

Utiliza la API de CHATGPT para pasar de voz a texto ==> CHATGPT ==> Emitir en voz y texto una respuesta

En este caso la temática es incendios.

Ideas:
    *Auxiliar de enfermería (social)(extinción)
    *Evacuación Gestor de Rutas
        *Edificios
            Cámaras
            Recuento de personas
            Ruta
            Descripción de la situación a los centros de ayuda
        *Vía aérea
            identico arriba ^
    *Ruta de abastecimientos de recursos para la extinción
    *Estudio de puntos calientes y peligrosos con vista aerea
    

En esta carpeta está el desarrollo del Auxiliar de enfermería

¡Claro! Para instalar las librerías necesarias que mencionas en tu código, puedes utilizar el comando `pip install` seguido del nombre de cada una de las librerías. Aquí tienes los comandos para instalar las librerías que mencionaste:

1. `pyttsx3`:
   ```
   pip install pyttsx3
   ```

2. `pyaudio`:
   ```
   pip install pyaudio
   ```

3. `wave`:
   No es necesario instalar esta librería por separado, ya que viene incluida en la biblioteca estándar de Python.

4. `speech_recognition`:
   ```
   pip install SpeechRecognition
   ```

5. `transformers`:
   ```
   pip install transformers
   ```

6. `torch`:
   ```
   pip install torch
   ```

Recuerda ejecutar estos comandos en tu entorno de desarrollo para instalar las librerías necesarias y poder utilizarlas en tu código. ¡Espero que esta información te sea de ayuda! Si tienes alguna otra pregunta, no dudes en decírmelo.


Para generar variables de entorno utiliza desde cmd: setx <Nombre de la variable> "mi_valor"
Para acceder desde python utiliza: os.environ['<Nombre de la variable>']