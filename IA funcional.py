import os
from openai import OpenAI


prompt_user = "Se me ha roto un brazo, que hago"




prompt_rol="Eres un enfermero, especialista en respuesta rápida en primeros auxilios. Tienes que ofrecer directrices para salvar a una persona en una urgencia. Responde de forma fluida como una persona y no me lo enumeres"

prompt= prompt_rol+prompt_user

client = OpenAI(
    api_key=os.environ['CGPT'],
)

chat_completion = client.chat.completions.create(
    messages=[
        {
            "role": "user",
            "content": prompt,
        }
    ],
    model="gpt-4", # "gpt-4", "gpt-3.5-turbo" # https://chat.openai.com/g/g-wC6qt2IZW-linguist-link
)

content = chat_completion.choices[0].message.content

print(content)