import tkinter as tk
from PIL import Image, ImageTk
import threading
import pyttsx3
import pyaudio
import wave
import speech_recognition as sr
from openai import OpenAI
import os

class Transcribir:
    def __init__(self, formato, canales, tasa_muestreo, tamanio_buffer, ruta_archivo):
        self.formato = formato
        self.canales = canales
        self.tasa_muestreo = tasa_muestreo
        self.tamanio_buffer = tamanio_buffer
        self.ruta_archivo = ruta_archivo
        self.grabando = False
        self.grabar_btn = None
        self.animating = False

    def salida_voz(self, texto):
        engine = pyttsx3.init()
        engine.say(texto)
        engine.runAndWait()

    def toggle_grabacion(self, cuadro_texto_input, cuadro_texto_output):
        if not self.grabando:
            self.grabando = True
            self.grabar_btn.config(text="Detener Grabación")
            self.animar_botones()
            threading.Thread(target=self.grabar_audio, args=(cuadro_texto_input, cuadro_texto_output)).start()
        else:
            self.grabando = False
            self.grabar_btn.config(text="Iniciar Grabación")

    def grabar_audio(self, cuadro_texto_input, cuadro_texto_output):
        try:
            audio = pyaudio.PyAudio()
            stream = audio.open(
                format=self.formato,
                channels=self.canales,
                rate=self.tasa_muestreo,
                input=True,
                frames_per_buffer=self.tamanio_buffer
            )

            frames = []

            while self.grabando:
                data = stream.read(self.tamanio_buffer)
                frames.append(data)

            stream.stop_stream()
            stream.close()
            audio.terminate()

            with wave.open(self.ruta_archivo, "wb") as wf:
                wf.setnchannels(self.canales)
                wf.setsampwidth(audio.get_sample_size(self.formato))
                wf.setframerate(self.tasa_muestreo)
                wf.writeframes(b"".join(frames))

            self.transcribir_audio(cuadro_texto_input, cuadro_texto_output)

        except Exception as e:
            print(f"Ha ocurrido un error al grabar el audio: {str(e)}")

    def transcribir_audio(self, cuadro_texto_input, cuadro_texto_output):
        try:
            r = sr.Recognizer()
            with sr.AudioFile(self.ruta_archivo) as audio_file:
                audio = r.record(audio_file)
            texto = r.recognize_google(audio, language="es-ES")

            if texto:
                cuadro_texto_input.insert(tk.END, texto + "\n")
                
                prompt_user = texto

                prompt_rol = "Eres un enfermero, especialista en respuesta rápida en primeros auxilios. Tienes que ofrecer directrices para salvar a una persona en una urgencia.Respondeen no más de 5-7 oraciones. Responde de forma fluida como una persona y no me lo enumeres"
                prompt = prompt_rol + prompt_user

                client = OpenAI(
                    api_key=os.environ['CGPT'],
                )

                chat_completion = client.chat.completions.create(
                    messages=[
                        {
                            "role": "user",
                            "content": prompt,
                        }
                    ],
                    model="gpt-4",
                )

                content = chat_completion.choices[0].message.content

                cuadro_texto_output.insert(tk.END, content + "\n")

                self.salida_voz(content)
                cuadro_texto_output.insert(tk.END, content + "\n")

            else:
                print("No se detectó texto en el audio")
                cuadro_texto_input.insert(tk.END, "No se ha detectado texto" + "\n")

        except sr.UnknownValueError:
            print("No se pudo transcribir el audio - No se detectó texto")
        except Exception as e:
            print(f"Ha ocurrido un error al transcribir el audio: {str(e)}")

    def animar_botones(self):
        if self.animating:
            return
        self.animating = True
        self.tamano_original = self.grabar_btn.winfo_width(), self.grabar_btn.winfo_height()
        self.animacion_botones()

    def animacion_botones(self, i=0):
        if self.grabando:
            if i % 2 == 0:
                self.grabar_btn.config(width=120, height=110)
            else:
                self.grabar_btn.config(width=self.tamano_original[0], height=self.tamano_original[1])
            self.grabar_btn.after(100, self.animacion_botones, i + 1)
        else:
            self.animating = False

def main():
    root = tk.Tk()
    root.title("Grabador de Audio")
    root.geometry("375x600")
    root.configure(bg="black")

    transcriptor = Transcribir(
        formato=pyaudio.paInt16,
        canales=2,
        tasa_muestreo=44100,
        tamanio_buffer=1048576,
        ruta_archivo="audio_grabacion.wav",
    )

    cuadro_texto_input = tk.Text(root, height=12, borderwidth=0, background="black", foreground="green", font=("Helvetica", 10, "bold"), padx=10, pady=10)
    cuadro_texto_input.pack()

    cuadro_texto_output = tk.Text(root,  height=12, borderwidth=0, background="black", foreground="green", font=("Helvetica", 10, "bold"), padx=10, pady=10)
    cuadro_texto_output.pack()

    imagen_btn = Image.open("button.png").convert("RGB")
    imagen_btn = imagen_btn.resize((100, 90))
    imagen_btn = ImageTk.PhotoImage(imagen_btn)

    transcriptor.grabar_btn = tk.Button(root, image=imagen_btn, command=lambda: transcriptor.toggle_grabacion(cuadro_texto_input, cuadro_texto_output), bd=0, bg="black")
    transcriptor.grabar_btn.pack(pady=20)

    def cerrar_ventana():
        if transcriptor.grabando:
            transcriptor.grabando = False
        root.destroy()

    root.protocol("WM_DELETE_WINDOW", cerrar_ventana)
    root.mainloop()

if __name__ == "__main__":
    main()
