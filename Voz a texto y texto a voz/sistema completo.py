import os
import threading
import tkinter as tk
import wave

import pyaudio
import pyttsx3
import speech_recognition as sr


class Transcribir:
    def __init__(self, formato, canales, tasa_muestreo, tamanio_buffer, ruta_archivo):
        self.formato = formato
        self.canales = canales
        self.tasa_muestreo = tasa_muestreo
        self.tamanio_buffer = tamanio_buffer
        self.ruta_archivo = ruta_archivo
        self.grabando = False
        self.thread = None

    @staticmethod
    def salida_voz(texto):
        engine = pyttsx3.init()
        voices = engine.getProperty('voices')
        engine.setProperty('voice', voices[0].id)
        engine.say(texto)
        engine.runAndWait()

    def toggle_grabacion(self):
        if not self.grabando:
            self.grabando = True
            self.thread = threading.Thread(target=self.grabar_audio)
            self.thread.start()
        else:
            self.grabando = False

    def grabar_audio(self):
        try:
            audio = pyaudio.PyAudio()
            stream = audio.open(
                format=self.formato,
                channels=self.canales,
                rate=self.tasa_muestreo,
                input=True,
                frames_per_buffer=self.tamanio_buffer
            )

            frames = []

            while self.grabando:
                data = stream.read(self.tamanio_buffer)
                frames.append(data)

            stream.stop_stream()
            stream.close()
            audio.terminate()

            with wave.open(self.ruta_archivo, "wb") as wf:
                wf.setnchannels(self.canales)
                wf.setsampwidth(audio.get_sample_size(self.formato))
                wf.setframerate(self.tasa_muestreo)
                wf.writeframes(b"".join(frames))

            self.transcribir_audio()
        except Exception as e:
            print(f"Ha ocurrido un error al grabar el audio: {str(e)}")

    def transcribir_audio(self):
        try:
            r = sr.Recognizer()
            with sr.AudioFile(self.ruta_archivo) as audio_file:
                audio = r.record(audio_file)
            texto = r.recognize_google(audio, language="es-ES")
            if texto:
                print("Texto transcrito:", texto)

                # Generar texto con parámetros ajustados
                # Función de la IA

                # Imprimir resultados
                
                Transcribir.salida_voz("Pon aquí lo de la IA")
                print("Respuesta")

                
            else:
                print("No se detectó texto en el audio")
        except sr.UnknownValueError:
            print("No se pudo transcribir el audio - No se detectó texto")
        except Exception as e:
            print(f"Ha ocurrido un error al transcribir el audio: {str(e)}")

#Cargar código base de la IA


# Crear la ventana tkinter
root = tk.Tk()
root.title("Grabador de Audio")

# Instancia de la clase Transcribir
transcriptor = Transcribir(
    formato=pyaudio.paInt16,
    canales=2,
    tasa_muestreo=44100,
    tamanio_buffer=65536,  # 2^16
    ruta_archivo="audio_grabacion.wav"
)

# Botón para iniciar/parar la grabación
grabar_btn = tk.Button(root, text="Iniciar Grabación", command=transcriptor.toggle_grabacion)
grabar_btn.pack(pady=10)

# Función para cerrar la ventana correctamente
def cerrar_ventana():
    if transcriptor.grabando:
        transcriptor.grabando = False
    root.destroy()

root.protocol("WM_DELETE_WINDOW", cerrar_ventana)
root.mainloop()