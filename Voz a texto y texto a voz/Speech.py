import tkinter as tk
from PIL import Image, ImageTk
import threading
import pyttsx3
import pyaudio
import wave
import speech_recognition as sr
import os
from transformers import AutoTokenizer, pipeline
import torch

class Transcribir:
    def __init__(self, formato, canales, tasa_muestreo, tamanio_buffer, ruta_archivo, generator):
        self.formato = formato
        self.canales = canales
        self.tasa_muestreo = tasa_muestreo
        self.tamanio_buffer = tamanio_buffer
        self.ruta_archivo = ruta_archivo
        self.generator = generator
        self.grabando = False
        self.grabar_btn = None  # Inicializamos el atributo para el botón de grabación
        self.animating = False

    def salida_voz(self, texto):
        # Inicializar el motor de síntesis de voz
        engine = pyttsx3.init()

        # Obtener la lista de voces disponibles en el sistema
        voices = engine.getProperty('voices')

        # Seleccionar una voz específica del sistema (por ejemplo, la primera voz de la lista)
        # Puedes cambiar el índice de la lista según la voz que quieras utilizar
        engine.setProperty('voice', voices[0].id)

        engine.say(texto)
        
        # Reproducir la voz
        engine.runAndWait()

    def toggle_grabacion(self):
        if not self.grabando:
            self.grabando = True
            self.grabar_btn.config(text="Detener Grabación")  # Usamos self.grabar_btn
            self.animar_botones()
            threading.Thread(target=self.grabar_audio).start()
        else:
            self.grabando = False
            self.grabar_btn.config(text="Iniciar Grabación")  # Usamos self.grabar_btn

    def grabar_audio(self):
        try:
            audio = pyaudio.PyAudio()
            stream = audio.open(
                format=self.formato,
                channels=self.canales,
                rate=self.tasa_muestreo,
                input=True,
                frames_per_buffer=self.tamanio_buffer
            )

            frames = []

            while self.grabando:
                data = stream.read(self.tamanio_buffer)
                frames.append(data)

            stream.stop_stream()
            stream.close()
            audio.terminate()

            with wave.open(self.ruta_archivo, "wb") as wf:
                wf.setnchannels(self.canales)
                wf.setsampwidth(audio.get_sample_size(self.formato))
                wf.setframerate(self.tasa_muestreo)
                wf.writeframes(b"".join(frames))

            self.transcribir_audio()
        except Exception as e:
            print(f"Ha ocurrido un error al grabar el audio: {str(e)}")

    def transcribir_audio(self):
        try:
            r = sr.Recognizer()
            with sr.AudioFile(self.ruta_archivo) as audio_file:
                audio = r.record(audio_file)
            texto = r.recognize_google(audio, language="es-ES")

            # Generar texto con parámetros ajustados
            sequences = self.generator(
                texto,
                do_sample=True,
                top_k=2,  # Reducción del top-k
                num_return_sequences=1,
                max_length=100,  # Reducción de la longitud máxima de las secuencias
                truncation=True
            )

            # Imprimir resultados
            if texto:
                print("Texto transcrito:")
                for seq in sequences:
                    print(f"{seq['generated_text']}")
                    self.salida_voz(f"{seq['generated_text']}")
            else:
                print("No se detectó texto en el audio")
        except sr.UnknownValueError:
            print("No se pudo transcribir el audio - No se detectó texto")
        except Exception as e:
            print(f"Ha ocurrido un error al transcribir el audio: {str(e)}")

    def animar_botones(self):
        if self.animating:
            return
        self.animating = True
        self.tamano_original = self.grabar_btn.winfo_width(), self.grabar_btn.winfo_height()  # Guardamos el tamaño original del botón
        self.animacion_botones()

    def animacion_botones(self, i=0):
        if self.grabando:
            if i % 2 == 0:
                self.grabar_btn.config(width=120, height=110)  # Ajusta el tamaño del botón
            else:
                self.grabar_btn.config(width=self.tamano_original[0], height=self.tamano_original[1])  # Restaura el tamaño original del botón
            self.grabar_btn.after(100, self.animacion_botones, i + 1)
        else:
            self.animating = False
def main():
    # Nombre del modelo que deseas cargar
    model_name = "meta-llama/Llama-2-7b-chat-hf"
    token = "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"  # Aquí debes colocar tu token
    language = "es"  # Idioma que deseas utilizar (cambia según el idioma deseado)

    # Cargar el tokenizer del modelo con truncation=True
    tokenizer = AutoTokenizer.from_pretrained(model_name, token=token, truncation=True, source=language)

    # Configurar el pipeline para generación de texto con GPU
    device = 0 if torch.cuda.is_available() else -1  # 0 para GPU, -1 para CPU
    generator = pipeline(
        "text-generation",
        model=model_name,
        tokenizer=tokenizer,
        device=device  # Utilizar GPU si está disponible
    )

    # Crear la ventana tkinter
    root = tk.Tk()
    root.title("Grabador de Audio")
    iphone_x_width = 375
    iphone_x_height = 600
    root.geometry(f"{iphone_x_width}x{iphone_x_height}")

    # Instancia de la clase Transcribir
    transcriptor = Transcribir(
        formato=pyaudio.paInt16,
        canales=2,
        tasa_muestreo=44100,
        tamanio_buffer=1048576,  # 2^16
        ruta_archivo="audio_grabacion.wav",
        generator=generator
    )

    root.configure(bg="black")  # Establecer color de fondo negro

    # Cargar la imagen del botón
    imagen_btn = Image.open("button.png").convert("RGB")
    imagen_btn = imagen_btn.resize((100, 90))  # Ajustar tamaño de la imagen
    imagen_btn = ImageTk.PhotoImage(imagen_btn)

    # Botón para iniciar/parar la grabación
    transcriptor.grabar_btn = tk.Button(root, image=imagen_btn, command=transcriptor.toggle_grabacion, bd=0, bg="black")
    transcriptor.grabar_btn.pack(pady=20)

    # Función para cerrar la ventana correctamente
    def cerrar_ventana():
        if transcriptor.grabando:
            transcriptor.grabando = False
        root.destroy()

    root.protocol("WM_DELETE_WINDOW", cerrar_ventana)
    root.mainloop()

if __name__ == "__main__":
    main()
