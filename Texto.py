import tkinter as tk

def agregar_texto():
    texto = entrada.get()  # Obtener el texto del campo de entrada
    cuadro_texto.insert(tk.END, texto + "\n")  # Agregar el texto al cuadro de texto
    entrada.delete(0, tk.END)  # Borrar el texto del campo de entrada

# Crear la ventana principal
ventana = tk.Tk()
ventana.title("Ejemplo de Cuadro de Texto con Tkinter")

# Crear el cuadro de texto
cuadro_texto = tk.Text(ventana)
cuadro_texto.pack()

# Crear el campo de entrada
entrada = tk.Entry(ventana)
entrada.pack()

# Crear el botón para agregar texto
boton_agregar = tk.Button(ventana, text="Agregar texto", command=agregar_texto)
boton_agregar.pack()

# Ejecutar el bucle principal
ventana.mainloop()

