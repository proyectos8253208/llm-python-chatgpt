import os
import time
import speech_recognition as sr
import pyaudio
import wave
import keyboard

class Transcribir:
    def __init__(
        self,
        formato: int,
        canales: int,
        tasa_muestreo: int,
        tamanio_buffer: int,
        ruta_archivo: str,
    ):
        self.formato = formato
        self.canales = canales
        self.tasa_muestreo = tasa_muestreo
        self.tamanio_buffer = tamanio_buffer
        self.ruta_archivo = ruta_archivo

    def transcribir(self):
        try:
            audio = pyaudio.PyAudio()
            stream = audio.open(
                format=self.formato,
                channels=self.canales,
                rate=self.tasa_muestreo,
                input=True,
                frames_per_buffer=self.tamanio_buffer
            )
            print("Esperando la tecla 'g' para empezar la grabación...")

            frames = []

            # Esperar la pulsación de la tecla 'g' para empezar la grabación
            keyboard.wait('g')
            print("Grabación empezada...")

            # Grabación de audio mientras la tecla 'g' esté pulsada
            while keyboard.is_pressed('g'):
                data = stream.read(self.tamanio_buffer)
                frames.append(data)

            print("Grabación finalizada")

            # Detener grabación y cerrar PyAudio
            stream.stop_stream()
            stream.close()
            audio.terminate()

            # Guardar la grabación
            with wave.open(self.ruta_archivo, "wb") as wf:
                wf.setnchannels(self.canales)
                wf.setsampwidth(audio.get_sample_size(self.formato))
                wf.setframerate(self.tasa_muestreo)
                wf.writeframes(b"".join(frames))

            resultado = self.transcribir_audio(self.ruta_archivo)
            if resultado["estado"] == "success":
                return {
                    "estado": "success",
                    "mensaje": "Proceso culminado con éxito",
                    "texto": resultado["texto"],
                }
            else:
                return {
                    "estado": "failed",
                    "mensaje": "No se pudo culminar con éxito el proceso",
                }
        except Exception as e:
            raise NameError(f"Ha ocurrido un error al grabar el audio, revisa: {str(e)}")

    def transcribir_audio(self, ruta_audio):
        try:
            r = sr.Recognizer()
            with sr.AudioFile(ruta_audio) as audio_file:
                audio = r.record(audio_file)
            texto = r.recognize_google(audio, language="es-ES")
            if texto:
                return {
                    "estado": "success",
                    "mensaje": "Audio transcrito de manera exitosa!",
                    "texto": texto,
                }
            else:
                return {
                    "estado": "failed",
                    "mensaje": "No se detectó texto en el audio",
                }
        except sr.UnknownValueError:
            return {
                "estado": "failed",
                "mensaje": "No se pudo transcribir el audio - No se detectó texto",
            }
        except Exception as e:
            raise NameError(f"Ha ocurrido un error al transcribir el audio, revisa: {str(e)}")

formato = pyaudio.paInt16
canales = 2
tasa_muestreo = 44100
tamanio_buffer=65536,#2^16
ruta_archivo = "audio_grabacion.wav"

transcriptor = Transcribir(formato, canales, tasa_muestreo, tamanio_buffer, ruta_archivo)
print(transcriptor.transcribir())
