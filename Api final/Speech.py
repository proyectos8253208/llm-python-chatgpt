import tkinter as tk
import threading
import pyttsx3
import pyaudio
import wave
import speech_recognition as sr
import os
from transformers import AutoTokenizer, pipeline
import torch

class Transcribir:
    def __init__(self, formato, canales, tasa_muestreo, tamanio_buffer, ruta_archivo, generator):
        self.formato = formato
        self.canales = canales
        self.tasa_muestreo = tasa_muestreo
        self.tamanio_buffer = tamanio_buffer
        self.ruta_archivo = ruta_archivo
        self.generator = generator
        self.grabando = False
        self.thread = None
        self.grabar_btn = None  # Inicializamos el atributo para el botón de grabación

    def salida_voz(self, texto):
        # Inicializar el motor de síntesis de voz
        engine = pyttsx3.init()

        # Obtener la lista de voces disponibles en el sistema
        voices = engine.getProperty('voices')

        # Seleccionar una voz específica del sistema (por ejemplo, la primera voz de la lista)
        # Puedes cambiar el índice de la lista según la voz que quieras utilizar
        engine.setProperty('voice', voices[0].id)

        engine.say(texto)
        
        # Reproducir la voz
        engine.runAndWait()

    def toggle_grabacion(self):
        if not self.grabando:
            self.grabando = True
            self.thread = threading.Thread(target=self.grabar_audio)
            self.thread.start()
            self.grabar_btn.config(text="Detener Grabación")  # Usamos self.grabar_btn
        else:
            self.grabando = False
            self.grabar_btn.config(text="Iniciar Grabación")  # Usamos self.grabar_btn

    def grabar_audio(self):
        try:
            audio = pyaudio.PyAudio()
            stream = audio.open(
                format=self.formato,
                channels=self.canales,
                rate=self.tasa_muestreo,
                input=True,
                frames_per_buffer=self.tamanio_buffer
            )

            frames = []

            while self.grabando:
                data = stream.read(self.tamanio_buffer)
                frames.append(data)

            stream.stop_stream()
            stream.close()
            audio.terminate()

            with wave.open(self.ruta_archivo, "wb") as wf:
                wf.setnchannels(self.canales)
                wf.setsampwidth(audio.get_sample_size(self.formato))
                wf.setframerate(self.tasa_muestreo)
                wf.writeframes(b"".join(frames))

            self.transcribir_audio()
        except Exception as e:
            print(f"Ha ocurrido un error al grabar el audio: {str(e)}")

    def transcribir_audio(self):
        try:
            r = sr.Recognizer()
            with sr.AudioFile(self.ruta_archivo) as audio_file:
                audio = r.record(audio_file)
            texto = r.recognize_google(audio, language="es-ES")

            # Generar texto con parámetros ajustados
            sequences = self.generator(
                texto,
                do_sample=True,
                top_k=2,  # Reducción del top-k
                num_return_sequences=1,
                max_length=20,  # Reducción de la longitud máxima de las secuencias
                truncation=True
            )

            # Imprimir resultados
            if texto:
                print("Texto transcrito:")
                for seq in sequences:
                    print(f"{seq['generated_text']}")
                    self.salida_voz(f"{seq['generated_text']}")
            else:
                print("No se detectó texto en el audio")
        except sr.UnknownValueError:
            print("No se pudo transcribir el audio - No se detectó texto")
        except Exception as e:
            print(f"Ha ocurrido un error al transcribir el audio: {str(e)}")

def main():
    # Nombre del modelo que deseas cargar
    model_name = "meta-llama/Llama-2-7b-chat-hf"
    token = "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"  # Aquí debes colocar tu token
    language = "es"  # Idioma que deseas utilizar (cambia según el idioma deseado)

    # Definir qué GPU utilizar (cambia el número según tu configuración)
    os.environ["CUDA_VISIBLE_DEVICES"] = "1"  # Utiliza la GPU 1 (Nvidia)

    # Cargar el tokenizer del modelo con truncation=True
    tokenizer = AutoTokenizer.from_pretrained(model_name, token=token, truncation=True, source=language)

    # Configurar el pipeline para generación de texto con GPU
    device = 0 if torch.cuda.is_available() else -1  # 0 para GPU, -1 para CPU
    generator = pipeline(
        "text-generation",
        model=model_name,
        tokenizer=tokenizer,
        device=device  # Utilizar GPU si está disponible
    )

    # Crear la ventana tkinter
    root = tk.Tk()
    root.title("Grabador de Audio")

    # Instancia de la clase Transcribir
    transcriptor = Transcribir(
        formato=pyaudio.paInt16,
        canales=2,
        tasa_muestreo=44100,
        tamanio_buffer=100000,#2^16
        ruta_archivo="audio_grabacion.wav",
        generator=generator
    )

    # Botón para iniciar/parar la grabación
    transcriptor.grabar_btn = tk.Button(root, text="Iniciar Grabación", command=transcriptor.toggle_grabacion)
    transcriptor.grabar_btn.pack(pady=10)  # Usamos transcriptor.grabar_btn

    # Función para cerrar la ventana correctamente
    def cerrar_ventana():
        if transcriptor.grabando:
            transcriptor.grabando = False
        root.destroy()

    root.protocol("WM_DELETE_WINDOW", cerrar_ventana)
    root.mainloop()

if __name__ == "__main__":
    main()
